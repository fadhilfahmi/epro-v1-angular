import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './/app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LayoutModule } from './/layouts/layout.module';
import { ScriptLoaderService } from './_services/script-loader.service';
import { ChartofaccountComponent } from './modules/setup/configuration/chartofaccount/chartofaccount.component';
import { HttpClientModule } from '@angular/common/http';
import { AppSidebarService } from './layouts/app-sidebar/app-sidebar.service';
import { ChartofaccountService } from './modules/setup/configuration/chartofaccount/chartofaccount.service';
import { AddChartofaccountComponent } from './modules/setup/configuration/chartofaccount/add-chartofaccount/add-chartofaccount.component';
import { EditChartofaccountComponent } from './modules/setup/configuration/chartofaccount/edit-chartofaccount/edit-chartofaccount.component';
import { ViewChartofaccountComponent } from './modules/setup/configuration/chartofaccount/view-chartofaccount/view-chartofaccount.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartofaccountComponent,
    AddChartofaccountComponent,
    EditChartofaccountComponent,
    ViewChartofaccountComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule
  ],
  providers: [AppSidebarService,ScriptLoaderService, ChartofaccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
