import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChartofaccountComponent } from './add-chartofaccount.component';

describe('AddChartofaccountComponent', () => {
  let component: AddChartofaccountComponent;
  let fixture: ComponentFixture<AddChartofaccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChartofaccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChartofaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
