import { Injectable, ChangeDetectorRef } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Chartofaccount } from './chartofaccount';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ChartofaccountService {

  constructor(private http: HttpClient, private chRef: ChangeDetectorRef) {}


  coa: Chartofaccount[];
  dataTable: any;

  public getListDatatable() {

    return this.http.get('http://localhost:8080/api/setup/configuration/coa/datatable')
      .subscribe((data: any[]) => {
        this.coa = data;
        this.chRef.detectChanges();
        const table:any = $('table');
        this.dataTable = table.DataTable({
          responsive: true,
          "bProcessing": true,
          "aaSorting": [[0, "asc"]],
          "aoColumns": [
            {"sWidth": "10%", "bSearchable": true},
            {"sWidth": "25%", "bSearchable": true},
            {"sWidth": "10%", "bSearchable": true},
            {"sWidth": "20%", "bSearchable": true},
            {"sWidth": "20%"}
          ],
          "aoColumnDefs": [{
                  "aTargets": [4],
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                      var a = '<button type="button" class="btn btn-primary btn-xs">Primary</button>';

                      $(nTd).empty();
                      $(nTd).attr("class", 'btn-group');
                      $(nTd).prepend(a);
                  }

              }]
        });

      });
  }

  

}


