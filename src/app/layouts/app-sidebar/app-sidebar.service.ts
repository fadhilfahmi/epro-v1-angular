import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Module } from './module';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AppSidebarService {

  constructor(private http:HttpClient) {}

  private userUrl = 'http://localhost:8080/api/system/modules/root';

  public getModule() {
    return this.http.get<Module[]>(this.userUrl);
  }

}


